/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.internal.StringMap;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import question_paper_generator.Database.Database;
import question_paper_generator.POJO.BranchPOJO;
import question_paper_generator.POJO.PreviousQuestionPaperPOJO;
import question_paper_generator.POJO.QuestionPOJO;
import question_paper_generator.POJO.SemesterPOJO;
import question_paper_generator.POJO.SubjectPOJO;
import question_paper_generator.QuestionPaperGenerator;

/**
 *
 * @author ADMIN
 */
public class JSONHelper {
    public static String convertQuestionsToJSON(HashMap<String, ArrayList<QuestionPOJO>> questionSets)
    {
        Gson gson = new Gson();
        return gson.toJson(questionSets);
    }
    
    public static HashMap<String, ArrayList<QuestionPOJO>> convertJSONToQuestions(String json, String type, String marks, String subject)
    {
        HashMap<String, ArrayList<QuestionPOJO>> temp = new HashMap<>();
        Gson gson = new Gson();
        HashMap<String, ArrayList<StringMap>> data = gson.fromJson(json, temp.getClass());
        
        for (Map.Entry<String, ArrayList<StringMap>> entry : data.entrySet()) {
            ArrayList<QuestionPOJO> questions = new ArrayList<>();
            for(StringMap questionMap: entry.getValue()) {
                JsonElement jsonElement = gson.toJsonTree(questionMap);
                QuestionPOJO question = gson.fromJson(jsonElement, QuestionPOJO.class);
                questions.add(question);
            }
            temp.put(entry.getKey(), questions);
        }
        
        if(type.equalsIgnoreCase("NORMAL")) {
            PDFHelper.generatePdf(type, Integer.parseInt(marks), subject, temp.get("5markQuestions"), temp.get("10markQuestions"));
        } else if(type.equalsIgnoreCase("MCQ")) {
            PDFHelper.generatePdf(type, Integer.parseInt(marks), subject, temp.get("1markQuestions"), temp.get("2markQuestions"));
        }
        return null;
    }
    
    public static void setJSONInDatabase(String table, String column, String json, HashMap<String, Object> details) {
        DependencyInjector di = QuestionPaperGenerator.di;
        Database db = (Database)di.get("database");
        DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
        HashMap<String, String> values = new HashMap<>();
        values.put(column, json);
        Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        values.put("created_at", date.toString());
        BranchPOJO branch = dh.getBranchFromName((String)details.get("branch"));
        SemesterPOJO semester = dh.getSemesterFromName((String)details.get("semester"));
        SubjectPOJO subject = dh.getSubjectFromName((String)details.get("subject"));
        values.put("semester_id", String.valueOf(semester.getId()));
        values.put("branch_id", String.valueOf(semester.getId()));
        values.put("subject_id", String.valueOf(semester.getId()));
        db.table(table).insert(values);
    }
}
