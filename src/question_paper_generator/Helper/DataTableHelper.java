/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import javax.swing.table.DefaultTableModel;

public class DataTableHelper 
{
    public static DefaultTableModel getMyTableModel(String [] colHeads, String editableCell)
    {
        DefaultTableModel model = new DefaultTableModel(colHeads,0){            
            @Override
            public boolean isCellEditable(int row,int col){
                return (editableCell.equals(colHeads[col]));
          }            
        };
        return model;
    }
}