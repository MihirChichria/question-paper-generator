/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.POJO;

public class SubjectPOJO { 
    public int id;
    public String name;
    public BranchPOJO branch;
    public SemesterPOJO semester;
    
    public SubjectPOJO(String name)
    {
        this.name = name;
    }
    
    public SubjectPOJO(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
    
    public SubjectPOJO(String name, BranchPOJO branch, SemesterPOJO semester) 
    {
        this.name = name;
        this.branch = branch;
        this.semester = semester;
    }
    
    public SubjectPOJO(int id, String name, BranchPOJO branch, SemesterPOJO semester) 
    {
        this.id = id;
        this.name = name;
        this.branch = branch;
        this.semester = semester;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public BranchPOJO getBranch()
    {
        return this.branch;
    }
    
    public SemesterPOJO getSemester()
    {
        return this.semester;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setBranch(BranchPOJO branch)
    {
        this.branch = branch;
    }
    
    public void setSemester(SemesterPOJO semester)
    {
        this.semester = semester;
    }
}
