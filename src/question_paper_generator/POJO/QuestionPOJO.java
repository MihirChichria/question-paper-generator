/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.POJO;

import java.util.ArrayList;

public class QuestionPOJO {
    public int id;
    public String title;
    public String difficultyLevel;
    public int marks;
    public ChapterPOJO chapter;
    public ArrayList<OptionPOJO> options;
    public String type;
    public int probability;
    
    public QuestionPOJO( String title, 
            String difficultyLevel, 
            int marks, 
            ChapterPOJO chapter, 
            String type
    ) {
        this.title = title;
        this.difficultyLevel = difficultyLevel;
        this.marks = marks;
        this.chapter = chapter;
        this.type = type;
    }
    
    public QuestionPOJO( String title, 
            String difficultyLevel, 
            int marks, 
            ChapterPOJO chapter, 
            ArrayList<OptionPOJO> options, 
            String type
    ) {
        this.title = title;
        this.difficultyLevel = difficultyLevel;
        this.marks = marks;
        this.chapter = chapter;
        this.options = options;
        this.type = type;
    }
    
    public QuestionPOJO(int id, 
            String title, 
            String difficultyLevel, 
            int marks, 
             String type, 
            int probability,
            ChapterPOJO chapter, 
            ArrayList<OptionPOJO> options
    ) {
        this.id = id;
        this.title = title;
        this.difficultyLevel = difficultyLevel;
        this.marks = marks;
        this.chapter = chapter;
        this.options = options;
        this.type = type;
        this.probability = probability;
    }
    
    public QuestionPOJO(int id, 
            String title, 
            String difficultyLevel, 
            int marks, 
            String type, 
            int probability,
            ChapterPOJO chapter
           
    ) {
        this.id = id;
        this.title = title;
        this.difficultyLevel = difficultyLevel;
        this.marks = marks;
        this.chapter = chapter;
        this.type = type;
        this.probability = probability;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getTitle()
    {
        return this.title;
    }
    
    public String getDifficultyLevel()
    {
        return this.difficultyLevel;
    }
    
    public int getMarks()
    {
        return this.marks;
    }
    
    public String getType()
    {
        return this.type;
    }
    
    public ChapterPOJO getChapter()
    {
        return this.chapter;
    }
    
    public int getProbability()
    {
        return this.probability;
    }
    
    public ArrayList<OptionPOJO> getOptions()
    {
        return this.options;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    public void setDifficultyLevel(String difficultyLevel)
    {
        this.difficultyLevel = difficultyLevel;
    }
    
    public void setMarks(int marks)
    {
        this.marks = marks;
    }
    
    public void setChapter(ChapterPOJO chapter)
    {
        this.chapter = chapter;
    }
    
    public void setOptions(ArrayList<OptionPOJO> options)
    {
        this.options = options;
    }
}
