/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.POJO;

public class ChapterPOJO {
    public int id;
    public String name;
    public int weight;
    public SubjectPOJO subject;
    
    public ChapterPOJO(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
    
    public ChapterPOJO(String name)
    {
        this.name = name;
    }
    
    public ChapterPOJO(String name, int weight, SubjectPOJO subject)
    {
        this.name = name;
        this.weight = weight;
        this.subject = subject;
    }
    
    public ChapterPOJO(int id, String name, int weight, SubjectPOJO subject)
    {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.subject = subject;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public int getWeight()
    {
        return this.weight;
    }
    
    public SubjectPOJO getSubject()
    {
        return this.subject;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setWeight(int weight)
    {
        this.weight = weight;
    }
    
    public void setSubject(SubjectPOJO subject)
    {
        this.subject = subject;
    }
}