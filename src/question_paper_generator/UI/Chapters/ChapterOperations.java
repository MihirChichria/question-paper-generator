/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI.Chapters;

import question_paper_generator.UI.Branches.*;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.POJO.BranchPOJO;
import question_paper_generator.POJO.ChapterPOJO;
import question_paper_generator.POJO.SemesterPOJO;
import question_paper_generator.POJO.SubjectPOJO;
import question_paper_generator.QuestionPaperGenerator;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;

public class ChapterOperations extends javax.swing.JPanel {

    /**
     * Creates new form CreateBranch
     */
    private DependencyInjector di;
    private String operation;
    private ChapterPOJO editableChapter;
    public ChapterOperations() {
        initComponents();
        di = QuestionPaperGenerator.di;
        setCreateOperation();
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        this.setVisible(true);
        loadData();
    }
    
    public ChapterOperations(ChapterPOJO editableChapter) {
        initComponents();
        this.setVisible(true);
        this.editableChapter = editableChapter;
        di = QuestionPaperGenerator.di;
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        setEditOperation();
        updateOperationLabel();
        loadData();
        setData();
    }
    
    private void setData() {
        this.txtName.setText(editableChapter.getName());
        this.txtWeight.setText(String.valueOf(editableChapter.getWeight()));
       this.branchComboBox.setSelectedItem(editableChapter.getSubject().getBranch().getName());
        this.semesterComboBox.setSelectedItem(editableChapter.getSubject().getSemester().getName());
        this.subjectComboBox.setSelectedItem(editableChapter.getSubject().getName());
    }
    
    private void loadData() {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        ArrayList<BranchPOJO> branches = dh.getBranches();
        ArrayList<SemesterPOJO> semesters = dh.getSemesters();
        
        for(BranchPOJO branch: branches) {
            branchComboBox.addItem(branch.getName());
        }
        for(SemesterPOJO semester: semesters) {
            semesterComboBox.addItem(semester.getName());
        }
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<String>();
        btnBack = new javax.swing.JLabel();
        operationLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSubmit = new javax.swing.JLabel();
        lblName1 = new javax.swing.JLabel();
        subjectComboBox = new javax.swing.JComboBox<String>();
        lblName2 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        semesterComboBox = new javax.swing.JComboBox<String>();
        lblName3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        branchComboBox = new javax.swing.JComboBox<String>();
        jSeparator5 = new javax.swing.JSeparator();
        lblName4 = new javax.swing.JLabel();
        txtWeight = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(89, 112, 129));
        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/back-button.png"))); // NOI18N
        btnBack.setToolTipText("");
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        operationLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        operationLabel.setForeground(new java.awt.Color(89, 112, 129));
        operationLabel.setText("Create Chapter");

        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));

        txtName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        txtName.setBorder(null);
        txtName.setDoubleBuffered(true);
        txtName.setMargin(new java.awt.Insets(0, 0, 0, 0));

        lblName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName.setText("Name");

        jSeparator1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnSubmit.setBackground(new java.awt.Color(89, 112, 129));
        btnSubmit.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnSubmit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSubmit.setText("Submit");
        btnSubmit.setToolTipText("");
        btnSubmit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(89, 112, 129)));
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });

        lblName1.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName1.setText("Branch");

        subjectComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        subjectComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Subject" }));
        subjectComboBox.setBorder(null);
        subjectComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblName2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName2.setText("Subject");

        jSeparator3.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        semesterComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        semesterComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Semester" }));
        semesterComboBox.setBorder(null);
        semesterComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        semesterComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                semesterComboBoxItemStateChanged(evt);
            }
        });

        lblName3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName3.setText("Semester");

        jSeparator4.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        branchComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        branchComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Branch" }));
        branchComboBox.setBorder(null);
        branchComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        branchComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                branchComboBoxItemStateChanged(evt);
            }
        });

        jSeparator5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblName4.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName4.setText("Weight");

        txtWeight.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        txtWeight.setBorder(null);
        txtWeight.setDoubleBuffered(true);
        txtWeight.setMargin(new java.awt.Insets(0, 0, 0, 0));

        jSeparator2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(operationLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblName)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(lblName3)
                                            .addGap(23, 23, 23)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(semesterComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jSeparator4)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGap(86, 86, 86)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(110, 110, 110)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(lblName1)
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(branchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblName2)
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(subjectComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(226, 226, 226)
                        .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName4)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtWeight, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(operationLabel)
                    .addComponent(btnBack))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(lblName3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(semesterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(branchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblName1))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(lblName2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(subjectComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName4)
                    .addComponent(txtWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
        int res = 0;
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");        
        if(!("".equals(txtName.getText()) && "".equals(txtWeight.getText())) && subjectComboBox.getSelectedIndex() != 0) {
            if("Create".equalsIgnoreCase(this.operation))
            {
                ChapterPOJO cp = new ChapterPOJO(txtName.getText(), Integer.parseInt(txtWeight.getText()),
                                  dh.getSubjectFromName((String)subjectComboBox.getSelectedItem())); 
                res = dh.insertChapter(cp);
            }                
            else if("Edit".equalsIgnoreCase(this.operation)) 
            {
                ChapterPOJO cp = new ChapterPOJO(editableChapter.getId(), txtName.getText(), 
                        Integer.parseInt(txtWeight.getText()),
                        dh.getSubjectFromName((String)subjectComboBox.getSelectedItem()));
                res = dh.updateChapter(cp);
            }
            UserInterfaceHelper.goBackToPanel(di, "chaptersIndex");
        }
    }//GEN-LAST:event_btnSubmitMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        UserInterfaceHelper.goBackToPanel(di, "chaptersIndex");
    }//GEN-LAST:event_btnBackMouseClicked

    private void branchComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_branchComboBoxItemStateChanged
        setSubjectComboBox(evt);
    }//GEN-LAST:event_branchComboBoxItemStateChanged

    private void semesterComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_semesterComboBoxItemStateChanged
        setSubjectComboBox(evt);
    }//GEN-LAST:event_semesterComboBoxItemStateChanged

    private void setSubjectComboBox(ItemEvent evt)
    {
        if(evt.getStateChange() == ItemEvent.DESELECTED) {
            subjectComboBox.removeAllItems();
            subjectComboBox.addItem("Select Subject");
        }
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            if(branchComboBox.getSelectedIndex() != 0 
                && semesterComboBox.getSelectedIndex() != 0)
            fetchSubjectDetails(branchComboBox.getSelectedItem().toString(), semesterComboBox.getSelectedItem().toString());
        }
    }
    private void fetchSubjectDetails(String branchName, String semesterName)
    {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        BranchPOJO branch = new BranchPOJO(branchName);
        SemesterPOJO semester = new SemesterPOJO(semesterName);
        ArrayList<SubjectPOJO> subjects = dh.getSubjectFromBranchAndSemester(branch, semester);
        for(SubjectPOJO subject: subjects) {
            subjectComboBox.addItem(subject.getName());
        }
    }

    private void setCreateOperation()
    {
        this.operation = "Create";
    }
    
    private void setEditOperation()
    {
        this.operation = "Edit";
    }
    
    private void updateOperationLabel()
    {
        operationLabel.setText(this.operation + " Chapter");
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> branchComboBox;
    private javax.swing.JLabel btnBack;
    private javax.swing.JLabel btnSubmit;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblName2;
    private javax.swing.JLabel lblName3;
    private javax.swing.JLabel lblName4;
    private javax.swing.JLabel operationLabel;
    private javax.swing.JComboBox<String> semesterComboBox;
    private javax.swing.JComboBox<String> subjectComboBox;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtWeight;
    // End of variables declaration//GEN-END:variables

}
