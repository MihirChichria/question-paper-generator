/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.QuestionPaperGenerator;

public class Index extends javax.swing.JFrame {

    private DependencyInjector di;
    public Index() {
        initComponents();
        this.di = QuestionPaperGenerator.di;
        this.setResizable(false);
        
        relatedPanel.setLayout(new CardLayout());
        addMouseListener(branchesPanel, branchesLabel, (JPanel)di.get("branchesIndex"));
        addMouseListener(semestersPanel, semestersLabel, (JPanel)di.get("semestersIndex"));
        addMouseListener(subjectsPanel, subjectsLabel, (JPanel)di.get("subjectsIndex"));
        addMouseListener(chaptersPanel, chaptersLabel, (JPanel)di.get("chaptersIndex"));
        addMouseListener(questionsPanel, questionsLabel, (JPanel)di.get("questionsIndex"));
        
        UserInterfaceHelper.addButtonMouseListener(branchesPanel, branchesLabel);
        UserInterfaceHelper.addButtonMouseListener(semestersPanel, semestersLabel);
        UserInterfaceHelper.addButtonMouseListener(subjectsPanel, subjectsLabel);
        UserInterfaceHelper.addButtonMouseListener(chaptersPanel, chaptersLabel);
        UserInterfaceHelper.addButtonMouseListener(questionsPanel, questionsLabel);
    }
    
    public JPanel getRelatedPanel()
    {
        return this.relatedPanel;
    }
    
    public void replacePanel(JPanel parentPanel, JPanel newPanel) {
        parentPanel.removeAll();
        parentPanel.repaint();
        parentPanel.add(newPanel);
        parentPanel.validate();
        parentPanel.revalidate();
        parentPanel.repaint();
    }
    
    private void addMouseListener(JPanel panel, JLabel label, JPanel nextRelatedPanel) {
        panel.addMouseListener(new MouseAdapter() {
            
         @Override
         public void mouseEntered(MouseEvent e) {
            panel.setBackground(new Color(89, 112, 129));
            label.setForeground(new Color(255, 255, 255));
            panel.validate();
         }
         
         @Override
         public void mouseExited(MouseEvent e) {
            panel.setBackground(new Color(255, 255, 255));
            label.setForeground(new Color(0, 0, 0));
            panel.validate();
         }
         
         @Override
         public void mouseClicked(MouseEvent e) {
            panel.setBackground(new Color(89, 112, 129));
            label.setForeground(new Color(255, 255, 255));
//            new Test().test();
            replacePanel(relatedPanel, nextRelatedPanel);
         }
      });

    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        branchesPanel = new javax.swing.JPanel();
        branchesLabel = new javax.swing.JLabel();
        semestersPanel = new javax.swing.JPanel();
        semestersLabel = new javax.swing.JLabel();
        subjectsPanel = new javax.swing.JPanel();
        subjectsLabel = new javax.swing.JLabel();
        chaptersPanel = new javax.swing.JPanel();
        chaptersLabel = new javax.swing.JLabel();
        questionsPanel = new javax.swing.JPanel();
        questionsLabel = new javax.swing.JLabel();
        relatedPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(153, 153, 153));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        branchesPanel.setBackground(new java.awt.Color(255, 255, 255));
        branchesPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        branchesLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 15)); // NOI18N
        branchesLabel.setText("Branches");

        javax.swing.GroupLayout branchesPanelLayout = new javax.swing.GroupLayout(branchesPanel);
        branchesPanel.setLayout(branchesPanelLayout);
        branchesPanelLayout.setHorizontalGroup(
            branchesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, branchesPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(branchesLabel)
                .addGap(47, 47, 47))
        );
        branchesPanelLayout.setVerticalGroup(
            branchesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, branchesPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(branchesLabel)
                .addContainerGap())
        );

        semestersPanel.setBackground(new java.awt.Color(255, 255, 255));
        semestersPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        semestersLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 15)); // NOI18N
        semestersLabel.setText("Semesters");

        javax.swing.GroupLayout semestersPanelLayout = new javax.swing.GroupLayout(semestersPanel);
        semestersPanel.setLayout(semestersPanelLayout);
        semestersPanelLayout.setHorizontalGroup(
            semestersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(semestersPanelLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(semestersLabel)
                .addContainerGap(40, Short.MAX_VALUE))
        );
        semestersPanelLayout.setVerticalGroup(
            semestersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(semestersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(semestersLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        subjectsPanel.setBackground(new java.awt.Color(255, 255, 255));
        subjectsPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        subjectsLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 15)); // NOI18N
        subjectsLabel.setText("Subjects");

        javax.swing.GroupLayout subjectsPanelLayout = new javax.swing.GroupLayout(subjectsPanel);
        subjectsPanel.setLayout(subjectsPanelLayout);
        subjectsPanelLayout.setHorizontalGroup(
            subjectsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, subjectsPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(subjectsLabel)
                .addGap(51, 51, 51))
        );
        subjectsPanelLayout.setVerticalGroup(
            subjectsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(subjectsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(subjectsLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        chaptersPanel.setBackground(new java.awt.Color(255, 255, 255));
        chaptersPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        chaptersLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 15)); // NOI18N
        chaptersLabel.setText("Chapters");

        javax.swing.GroupLayout chaptersPanelLayout = new javax.swing.GroupLayout(chaptersPanel);
        chaptersPanel.setLayout(chaptersPanelLayout);
        chaptersPanelLayout.setHorizontalGroup(
            chaptersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chaptersPanelLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(chaptersLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        chaptersPanelLayout.setVerticalGroup(
            chaptersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chaptersPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chaptersLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        questionsPanel.setBackground(new java.awt.Color(255, 255, 255));
        questionsPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        questionsLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 15)); // NOI18N
        questionsLabel.setText("Questions");

        javax.swing.GroupLayout questionsPanelLayout = new javax.swing.GroupLayout(questionsPanel);
        questionsPanel.setLayout(questionsPanelLayout);
        questionsPanelLayout.setHorizontalGroup(
            questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionsPanelLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(questionsLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        questionsPanelLayout.setVerticalGroup(
            questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(questionsLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(branchesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(semestersPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(subjectsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(chaptersPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(questionsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(branchesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(semestersPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(subjectsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chaptersPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(questionsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 185, Short.MAX_VALUE))
        );

        relatedPanel.setBackground(new java.awt.Color(255, 255, 255));
        relatedPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout relatedPanelLayout = new javax.swing.GroupLayout(relatedPanel);
        relatedPanel.setLayout(relatedPanelLayout);
        relatedPanelLayout.setHorizontalGroup(
            relatedPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 510, Short.MAX_VALUE)
        );
        relatedPanelLayout.setVerticalGroup(
            relatedPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(relatedPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(relatedPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

  
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel branchesLabel;
    private javax.swing.JPanel branchesPanel;
    private javax.swing.JLabel chaptersLabel;
    private javax.swing.JPanel chaptersPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel questionsLabel;
    private javax.swing.JPanel questionsPanel;
    private javax.swing.JPanel relatedPanel;
    private javax.swing.JLabel semestersLabel;
    private javax.swing.JPanel semestersPanel;
    private javax.swing.JLabel subjectsLabel;
    private javax.swing.JPanel subjectsPanel;
    // End of variables declaration//GEN-END:variables
}
