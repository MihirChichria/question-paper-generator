/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI.Semesters;

import question_paper_generator.UI.Chapters.*;
import question_paper_generator.UI.Branches.*;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.POJO.SemesterPOJO;
import question_paper_generator.QuestionPaperGenerator;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;

public class SemesterOperations extends javax.swing.JPanel {

    /**
     * Creates new form CreateBranch
     */
    private DependencyInjector di;
    private String operation;
    private SemesterPOJO editableSemester;
    public SemesterOperations() {
        initComponents();
        di = QuestionPaperGenerator.di;
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        setCreateOperation();
        this.setVisible(true);
    }
    
    public SemesterOperations(SemesterPOJO editableSemester) {
        initComponents();
        this.editableSemester = editableSemester;
        di = QuestionPaperGenerator.di;
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        setEditOperation();
        updateOperationLabel();
        this.txtName.setText(editableSemester.getName());
        this.setVisible(true);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<String>();
        btnBack = new javax.swing.JLabel();
        operationLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSubmit = new javax.swing.JLabel();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(89, 112, 129));
        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/back-button.png"))); // NOI18N
        btnBack.setToolTipText("");
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        operationLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        operationLabel.setForeground(new java.awt.Color(89, 112, 129));
        operationLabel.setText("Create Semester");

        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));

        txtName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        txtName.setBorder(null);
        txtName.setDoubleBuffered(true);
        txtName.setMargin(new java.awt.Insets(0, 0, 0, 0));

        lblName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName.setText("Name");

        jSeparator1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnSubmit.setBackground(new java.awt.Color(89, 112, 129));
        btnSubmit.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnSubmit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSubmit.setText("Submit");
        btnSubmit.setToolTipText("");
        btnSubmit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(89, 112, 129)));
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(operationLabel))
                            .addComponent(lblName)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(226, 226, 226)
                        .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(238, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(operationLabel)
                    .addComponent(btnBack))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 152, Short.MAX_VALUE)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
        int res = 0;
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        if(! "".equals(txtName.getText())) {
            if("Create".equalsIgnoreCase(this.operation))
            {
                SemesterPOJO semester = new SemesterPOJO(txtName.getText());
                res = dh.insertSemester(semester);
            }                
            else if("Edit".equalsIgnoreCase(this.operation)) 
            {
                SemesterPOJO semester = new SemesterPOJO(editableSemester.getId(), txtName.getText());
                res = dh.updateSemester(semester);
            }
            UserInterfaceHelper.goBackToPanel(di, "semestersIndex");
        }
    }//GEN-LAST:event_btnSubmitMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        UserInterfaceHelper.goBackToPanel(di, "semestersIndex");
    }//GEN-LAST:event_btnBackMouseClicked

    private void setCreateOperation()
    {
        this.operation = "Create";
    }
    
    private void setEditOperation()
    {
        this.operation = "Edit";
    }
    
    private void updateOperationLabel()
    {
        operationLabel.setText(this.operation + " Semester");
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnBack;
    private javax.swing.JLabel btnSubmit;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel operationLabel;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

}
