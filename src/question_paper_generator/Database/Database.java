/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Database {
    public Connection conn;
    public PreparedStatement ps;
    public ResultSet rs;
    public Statement stmt;
    
    public String table;
    public Database(){
        conn = MySQLConnect.getConnection();
    }
    
    public ResultSet query(String query){
        try {
            stmt = conn.createStatement();
            return stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error while executing Query: " + ex.getMessage());
        }
        return null;
    }
    
    public void queryUpdate(String query){
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Database table(String table){
        this.table = table;
        return this;
    }
    
    public int insert(HashMap<String, String> keyValue){
        ArrayList keys = new ArrayList(keyValue.keySet());
        ArrayList values = new ArrayList(keyValue.values());
        
        String psString = "?";
        for(int i=0; i<keys.size()-1; i++){
            psString += ", ?";
        }
        String sql = "INSERT INTO "+this.table+"("+implodeFromList(keys)+")" + " VALUES ("+psString+")";
        try {
            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            for(int i=0; i<values.size(); i++){
                ps.setString(i+1, (String)values.get(i)); 
            }
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int last_inserted_id = 0;
            if(rs.next())
            {
                 last_inserted_id= rs.getInt(1);
            }
            return last_inserted_id;
        }catch(SQLException e){
            System.out.println("Error While Inserting: " + e.getMessage());
        }
        return 0;
    }
    
    public int update(HashMap<String, String> keyValue, String condition){
        ArrayList<String> keys = new ArrayList<>(keyValue.keySet());
        ArrayList<String> values = new ArrayList<>(keyValue.values());
        
        String setString = "";
        int count = 0;
        for(String key: keys) {
            count++;
            if(count == keys.size()) {
                setString += key + " = ? ";
            } else {
                setString += key + " = ?, ";
            }
        }
        String sql = "UPDATE "+this.table + " SET " +  setString + " WHERE " + condition;
        try {
            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            for(int i=0; i<values.size(); i++){
                ps.setString(i+1, (String)values.get(i)); 
            }
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int last_inserted_id = 0;
            if(rs.next())
            {
                 last_inserted_id= rs.getInt(1);
            }
            return last_inserted_id;
        }catch(SQLException e){
            System.out.println("Error While Inserting: " + e.getMessage());
        }
        return 0;
    }
 
    public boolean exists(String field, String value){
        return this.where(field, "=", value).count() != 0;
    }
    
    public int count(){
        try {
            rs = this.get();
            if(rs == null)
                return 0;
            int count = 0;
            while(rs.next()){
                count++;
            }
            return count;
        } catch (SQLException ex) {
            System.out.println("Error in count: "+ex.getMessage());
        }
        return 0;
    }
    
    public Database where(String operation, String ...conditions) {
        String[] values = new String[conditions.length];
        int valueCount = 0;
        String[] temp;
        String queryString = "", preparedString;
        
        for(int i=0; i<conditions.length; ++i) {
            temp = conditions[i].split(" ", 3);
            preparedString = " ? ";
            if(temp[2].contains("~")) {
                preparedString = " " + temp[2].substring(temp[2].indexOf("~")+1) +" ";
                
            } else {
                values[valueCount++] = temp[2];
            }
            if(i != 0) {
                queryString += operation + " " + temp[0] + " " + temp[1] + preparedString;
            } else {
                queryString += " WHERE " + temp[0] + " " + temp[1] + preparedString;
            }
            
        }
        try {
            String query = "SELECT * FROM " + this.table + queryString;
            ps = conn.prepareStatement(query);
            for(int i=0; i<valueCount; ++i) {
                ps.setString(i+1, values[i]);
            }
            return this;
        } catch (SQLException ex) {
            System.out.println("Error While Executing Where: " + ex.getMessage());
        }
        return null;
    }
    
    public String implodeFromList(ArrayList list){
        if(list.isEmpty())
            return "";
        else{
            StringBuffer sb = new StringBuffer();
            sb.append(list.get(0));
            for(int i=1; i<list.size(); i++){
                sb.append(", ");
                sb.append(list.get(i));
            }
            return sb.toString();
        }
    }
    
    public String implodeFromListWithQuotes(ArrayList list){
        if(list.isEmpty())
            return "";
        else{
            StringBuffer sb = new StringBuffer();
            sb.append("'" + list.get(0) + "'");
            for(int i=1; i<list.size(); i++){
                sb.append(", ");
                sb.append("'" + list.get(i) + "'");
            }
            return sb.toString();
        }
    }
    
    public void delete(String condition) {
        String sql = "DELETE FROM "+this.table+" WHERE "+ condition;
        try {
            ps = conn.prepareStatement(sql);
            ps.execute();
        }catch(SQLException e){
            System.out.println("Error While Inserting: " + e.getMessage());
        }
    }
    
    public ResultSet get(){
        try {
            return ps.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Error in get: " + ex.getMessage());
        }
        return null;
    }
}
